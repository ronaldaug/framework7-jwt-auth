import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export const store = new Vuex.Store({
    state:{
        currentUser:{},
        isLoggedIn:false,
    },
    mutations:{
        loginUser(state,payload){
            store.state.currentUser = payload;
        },
        isloggedIn(state,payload){
            store.state.isLoggedIn = payload;
        },
        logoutUser(state,payload){
            localStorage.removeItem('cool-jwt');
            localStorage.removeItem('cool-user');
            store.state.currentUser = payload;
        }
    },
    actions:{

    },
    getters:{
        checkLogin(state){
           return state.isLoggedIn;
        }
    }
})