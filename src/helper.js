import Vue from 'vue';
Vue.mixin({
    methods: {
        validatePassword(pass){
    if (pass.length < 6) {
        this.errors.push("Your password must be at least 6 characters");
    }
    if (pass.search(/[a-z]/i) < 0) {
        this.errors.push("Your password must contain at least one letter."); 
    }
    if (pass.search(/[0-9]/) < 0) {
        this.errors.push("Your password must contain at least one digit.");
    }
    if (this.errors.length > 0) {
        return false;
        }
      return true;
    }
    }
})