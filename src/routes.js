import HomePage from './pages/home.vue';
import NotFoundPage from './pages/not-found.vue';
import LoginPage from './pages/login.vue';
import RegisterPage from './pages/register.vue';
import PanelLeftPage from './pages/panel-left.vue';
import {store} from './store';
const jwt = localStorage.getItem('cool-jwt');
const user = localStorage.getItem('cool-user');
if(jwt && user){
  store.commit("isloggedIn", true)
}else{
  store.commit("isloggedIn", false)
}

const router = [
    {
      path: '/login/',
      name: "Login page",
      component: LoginPage
    },
    {
      path: '/signup/',
      name: "Signup page",
      component: RegisterPage
    },
    {
      path: '/home/',
      component:HomePage
    },
    {
        path:'/panel-left/',
        name: "Panel left page",
        component:PanelLeftPage,
    },
    {
      path: '(.*)',
      name: "Not found page",
      component: NotFoundPage,
    },
  ];

export default router
